const express = require('express')
const db1 = require('./db')
const utils = require('./utils')
const router = express.Router()

router.get('/',(req,res)=> {
    const stmt = `select * from Movie`
    db1.execute(stmt,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})
router.post('/',(req,res)=> {
    const { title, releaseDate, duration, director} = req.body
    const stmt = `insert into movie (movie_title,movie_release_date,movie_time,director_name) values ('${title}','${releaseDate}',${duration},'${director}')`
    db1.execute(stmt,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})
router.put('/:id',(req,res)=> {
    const { id } = req.params
    const { releaseDate, duration} = req.body
    const stmt = `update movie set movie_release_date = '${releaseDate}' and movie_time = ${duration} where movie_id = ${id}`
    db1.execute(stmt,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})
router.delete('/:id',(req,res)=> {
    const { id } = req.params
    const stmt = `delete from movie where movie_id = ${id}`
    db1.execute(stmt,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})


module.exports = router