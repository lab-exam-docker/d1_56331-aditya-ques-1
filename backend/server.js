const express = require('express')
const movRouter = require('./movie')

const app = express()

app.use(express.json())
app.use('/movie', movRouter)

app.listen(4000, () => {
    console.log('server started on port 4000')
})