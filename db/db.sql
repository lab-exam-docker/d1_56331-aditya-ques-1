create table Movie (
    movie_id int primary key auto_increment,
    movie_title varchar(100),
    movie_release_date Date,
    movie_time int, 
    director_name varchar(100)
);

insert into Movie (movie_title,movie_release_date,movie_time,director_name) values ('Pushpa','2022-01-01',180,'abcd')